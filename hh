#! /usr/local/bin/python

import orgmodify 


root=orgmodify.load('/home/andy/org/Haushalt.org')
shopped = root.searchtag("in")[0]
storage = root.searchtag("storage")[0]
used = root.searchtag("out")[0]

#----------------------- Verbrauch --------------------------------------------

not_found=[]
for line in used.body.splitlines():
    print(line)
    words= line.strip().split(" ",1)
    if words[0].isnumeric():
        barcode= words[0]
        product = storage.search_in_property("BARCODE",barcode)
        if product:
            amount = str(int(product[0].properties.setdefault("AMOUNT","0"))-1)
            product[0].properties["AMOUNT"] = amount
        else:
            if len(words) > 1:
                found=False
                for product in storage.children:
                    if words[1] in product.heading:
                        product.properties["BARCODE"] = f"{product.properties['BARCODE']}, {barcode}"
                        amount = str(int(product[0].properties.setdefault("AMOUNT","0"))-1)
                        product[0].properties["AMOUNT"] = amount
                        found = True
                if not found:        
                    product = orgmodify.OrgNode(root.env)
                    product._tags = []
                    product._lines = [""]

                    product._heading = words[1]
                    product.properties["BARCODE"] = barcode
                    product.properties["AMOUNT"] = "0"
                    product.insert(storage)
            else:
                not_found.append(line)
    else:
        not_found.append(line)
used._body_lines = not_found
        

#----------------------- Einkauf --------------------------------------------
not_found=[]
for line in shopped.body.splitlines():
    print(line)
    words= line.strip().split(" ",1)
    if words[0].isnumeric():
        barcode= words[0]
        product = storage.search_in_property("BARCODE",barcode)
        if product:
            amount = str(int(product[0].properties.setdefault("AMOUNT","0"))+1)
            product[0].properties["AMOUNT"] = amount
        else:
            if len(words) > 1:
                found=False
                for product in storage.children:
                    if words[1] in product.heading:
                        product.properties["BARCODE"] = f"{product.properties['BARCODE']}, {barcode}"
                        amount = str(int(product[0].properties.setdefault("AMOUNT","0"))+1)
                        product[0].properties["AMOUNT"] = amount
                        found = True
                if not found:        
                    product = orgmodify.OrgNode(root.env)
                    product._tags = []
                    product._lines = [""]
                    product._heading = words[1]
                    product.properties["BARCODE"] = barcode
                    product.properties["AMOUNT"] = "1"
                    product.insert(storage)
            else:
                not_found.append(line)
    else:
        not_found.append(line)
shopped._body_lines = not_found
#----------------------- Einkaufsliste --------------------------------------------
shoppinglists = root.searchtag("shoppinglists")[0]

shoppinglist ={}
for article in storage.children:
    amount =  int(article.properties["AMOUNT"]) 
    minimum =  int(article.properties.setdefault("MIN","0"))
    buy =  int(article.properties.setdefault("BUY","1"))
    where =  article.properties.setdefault("SHOP","TEUER")
    if amount < minimum:
            shoplist = shoppinglist.get(where,[])
            shoplist.append(f" - [X] {article.heading} ({minimum-amount})" if buy == 1 else f" - [X] {article.heading} ({buy * (int(minimum / buy) +1)})")
            shoppinglist[where] = shoplist
body = []
for shop , articles in shoppinglist.items():
    body.append(f"** {shop}                                                       :shop:")
    for article in articles:
        body.append(article)

for shoppinglist in shoppinglists.children:
    shoppinglist.delete()
shoppinglists._body_lines = body

root.save()

